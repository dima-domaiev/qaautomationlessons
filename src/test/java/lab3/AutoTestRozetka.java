package lab3;


import lab3.rozetkaPages.TestRozetkaBasket;
import lab3.rozetkaPages.TestRozetkaCities;
import lab3.rozetkaPages.TestRozetkaMain;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by dinamik on 6/1/2016.
 */
public class AutoTestRozetka {
    private static WebDriver driver;
    protected static TestRozetkaBasket rozetkaBasket;
    protected static TestRozetkaCities rozetkaCities;
    protected static TestRozetkaMain rozetkaMain;

    @BeforeClass
    public static void SetUp() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        rozetkaBasket = new TestRozetkaBasket(driver);
        rozetkaCities = new TestRozetkaCities(driver);
        rozetkaMain = new TestRozetkaMain(driver);
    }

    @Before
    public void setUpTest() {
        driver.get("http://rozetka.com.ua/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void tearDown() {
        driver.close();
    }

    @Test
    public void testMainPage() {
        rozetkaMain.closeRozetkaSubskribes();
        rozetkaMain.checkRozetkaLogoOnMainPage_isDispl();
        rozetkaMain.checkAppleInMenu_isDispl();
        rozetkaMain.checkMP3InMenu_isDispl();
        rozetkaMain.rozetkaCities_isDispl();
        rozetkaMain.checkThatBasket_isDispl();

    }

    @Test
    public void testcitiesPopup() {
        rozetkaMain.closeRozetkaSubskribes();
        rozetkaMain.clickCitiesLink();
        rozetkaCities.citiesPopup_isDispl();
        rozetkaCities.citiesPopup_Contains("Киев");
        rozetkaCities.citiesPopup_Contains("Одесса");
        rozetkaCities.citiesPopup_Contains("Харьков");
        rozetkaCities.closePopupSelCity_Click();

    }

    @Test
    public void testBusketIsEmpty() {
        rozetkaMain.navigateToTheBasket();
        rozetkaBasket.checkThatBusketIsEmpty();
    }


}
