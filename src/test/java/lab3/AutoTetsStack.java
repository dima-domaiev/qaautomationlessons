package lab3;

import lab3.stackowerflowPages.SingUp;
import lab3.stackowerflowPages.StMain;
import lab3.stackowerflowPages.TopQuestionPage;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by dinamik on 6/6/2016.
 */
public class AutoTetsStack {
    public WebDriver driver;
    public StMain main;
    public SingUp singUp;
    public TopQuestionPage qPage;

    @Before
    public void setUptest() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        main = new StMain(driver);
        singUp = new SingUp(driver);
        qPage = new TopQuestionPage(driver);
        driver.get("http://stackoverflow.com/");

        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void downTest() {
        driver.close();
    }

    @Test
    public void bountyIndicator() {
        main.checkBounty(300);

    }

    @Test
    public void singUpChecking() {
        main.buttonSingUp.isDisplayed();
        main.navigateToSingUpPage();
        singUp.googleButton();
        singUp.facebookButton();
    }

    @Test
    public void topQuestion() {
        main.navigateToQuestionPage();
        qPage.checkQuastionDay();
    }

    @Test
    public void checkSalary() {
        main.checkSalaryMoreThen100k();
    }
}
