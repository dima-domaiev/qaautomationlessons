package lab2;

import org.junit.*;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dinamik on 5/25/2016.
 */
public class TestStackoverflow {
    private static WebDriver driver;
    private static String baseUrl = "http://stackoverflow.com/";

    @BeforeClass
    public static void main() {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    public static void end() {
        driver.quit();
    }

    @Before
    public void setUp() throws Exception {
        driver.get(baseUrl);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @Test
    public void featured() throws InterruptedException {
        //a
        String value = driver.findElement(By.xpath(".//*[@id='tabs']/a/span")).getText();
        Assert.assertTrue("failed", (Integer.parseInt(value) > 300));
    }

    @Test
    public void checkButtonsGoogleAndFacebookIsPresent() {
        //b
        WebElement buttonSingUp = driver.findElement(By.xpath(".//*[@id='tell-me-more']"));
        Assert.assertTrue("Sing Up button is not found", buttonSingUp.isDisplayed());
        buttonSingUp.click();
        WebElement loginFromGoogle = driver.findElement(By.xpath(".//*[contains(text(),'Google')]/.."));
        Assert.assertTrue("loginFromGoogle button is not found", loginFromGoogle.isDisplayed());
        WebElement loginFromFacebook = driver.findElement(By.xpath(".//*[contains(text(),'Facebook')]/.."));
        Assert.assertTrue("loginFromFacebook button is not found", loginFromFacebook.isDisplayed());
    }

    @Test
    public void checkThatTopQuestionsIsToday() {
        //c
        WebElement rTime = driver.findElement(By.xpath(".//*[@class='relativetime']/../../../h3/a")); //questionHyperlink
        rTime.click();
        WebElement asked = driver.findElement(By.xpath(".//*[contains(@title,'2016')]/b"));
        Assert.assertTrue("nezhdano4ka", asked.getText().equals("today"));
    }

    @Test
    public void checkSalaryMoreThen100k() {
        String salary = driver.findElement(By.xpath(".//*[@class='title']")).getText();

        Pattern p = Pattern.compile("-?\\d+");
        Matcher m = p.matcher(salary);
        while (m.find()) {
            System.out.println(m.group());
            salary = m.group();
        }
        int i = Integer.parseInt(salary);
        Assert.assertTrue("less then 100", i > 100);
    }
}
