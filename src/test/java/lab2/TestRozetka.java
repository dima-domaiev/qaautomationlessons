package lab2;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by dinamik on 5/25/2016.
 */
public class TestRozetka {
    private WebDriver driver;
    private String baseUrl= "http://rozetka.com.ua/";

    @Before
    public void setUp() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get(baseUrl);
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @After
    public void teatDown() throws Exception {
        System.out.println("Тест пройден успешно!");
        driver.quit();
    }

    @Test
    public void checkRozetkaLogoIsPresentOnMainPage() throws InterruptedException {
        //a
        Assert.assertTrue("rozetka Logo is not found",driver.findElement(By.xpath(".//*[contains(@src,'99.png')]")).isDisplayed()); //Проверка лого розетка в левом верхнем углу главной страницы
    }

    @Test
    public void checkAppleInMenuPresent()
    {
        //b
        Assert.assertTrue("Apple is not present in menu",driver.findElement(By.xpath(".//*[@id='m-main']/li[contains(a,'Apple')]/a")).isDisplayed()); //Проверка ,что меню каталога товаров содержит пункт ‘Apple’
    }

    @Test
    public void checkMP3InMenuPresent() throws InterruptedException {
        //c
        Assert.assertTrue("MP3 is not present in menu",driver.findElement(By.xpath(".//*[@id='m-main']/li[contains(a,'MP3')]/a")).isDisplayed()); //Проверка ,что меню каталога товаров содержит пункт 'MP3'
    }

    @Test
    public void checkSelectCitiesIsPresent() throws InterruptedException {
        //d
        WebElement popupNearCitiesLink = driver.findElement(By.xpath(".//*[@id='SubscribePushNotificationPanel']"));
        WebElement closePopupNearCitiesLink = driver.findElement(By.xpath(".//*[@id='SubscribePushNotificationPanel']/div/div[3]"));
        if (popupNearCitiesLink.isDisplayed()){
            closePopupNearCitiesLink.click();
        }
        driver.findElement(By.xpath(".//*[@id='city-chooser']/a/span")).click(); //Клик на пункт "Выберите город"
        Assert.assertTrue("pop-up Select cities is not present in menu",driver.findElement(By.xpath(".//*[@id='city-chooser']/div/div")).isDisplayed());//Проверка, что попап выбора города отображается
        Assert.assertTrue("Харьков is not present in Select cities pop-up",driver.findElement(By.xpath(".//*[@locality_id='31']")).isDisplayed()); //Проверка линки на Харьков
        Assert.assertTrue("Одесса is not present in Select cities pop-up",driver.findElement(By.xpath(".//*[@locality_id='30']")).isDisplayed()); //Проверка линки на Одессу
        Assert.assertTrue("Киев is not present in Select cities pop-up",driver.findElement(By.xpath(".//*[@locality_id='1']")).isDisplayed()); //Проверка линки на Киев
        driver.findElement(By.xpath(".//*[@id='city-chooser']/div/div/a/img")).click();//Закрытие попапа выбора городов
    }

    @Test
    public void checkThatBasketIsPresent() throws InterruptedException {
        //e
        driver.findElement(By.xpath(".//*[contains(@id,'cart_block')]/a")).click(); //Корзина
        Assert.assertTrue("Basket is not empty",driver.findElement(By.xpath(".//*[@id='drop-block']/h2")).isDisplayed()); //Проверка что карзина пустая
    }
}
