package lectures.lab4.steps;

import lectures.lab4.runner.RunForNixBlog;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.By;

/**
 * Created by student on 6/2/2016.
 */
public class MyStepdefs1 {
    @Given("^I on nix start page$")
    public void iOnNixStartPage() throws Throwable {
        if (RunForNixBlog.driver.getTitle().equals("NIX Solutions – Outsourcing Offshore Software Development Company"));
    }

    @When("^I perform click on blog linc$")
    public void iPerformClickOnBlogLinc() throws Throwable {
        RunForNixBlog.driver.findElement(By.xpath(".//*[@id='menu-item-6321']/a")).click();
    }

    @Then("^I see (\\d+) results articles$")
    public void iSeeResultsArticles(int countOfArticles) throws Throwable {
        int count = RunForNixBlog.driver.findElements(By.xpath(".//html/body/div[1]/div[2]/div/div[1]/div[1]")).size();
        Assert.assertTrue("We don't see "+countOfArticles+" articles on a page. size is "+count+"", count == countOfArticles);
    }

    @And("^I see label \"([^\"]*)\"$")
    public void iSeeLabel(String arg0) throws Throwable {
       String Blog = RunForNixBlog.driver.findElement(By.xpath(".//h1")).getText();
        Assert.assertTrue("Blog label is not found", Blog.contains(arg0));
    }
}
