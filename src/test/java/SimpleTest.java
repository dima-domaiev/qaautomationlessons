import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;

import java.util.concurrent.TimeUnit;

/**
 * Created by dinamik on 5/23/2016.
 */
public class SimpleTest {
    @Test
    public void navigateToGoogle(){
        WebDriver driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.get("https://ya.ru");

        WebElement searchField = driver.findElement(By.xpath(".//*[@id='text']"));
        searchField.clear();
        searchField.sendKeys("Automation it is cool!");
        driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);

        WebElement searchButton = driver.findElement(By.xpath("html/body/table/tbody/tr[2]/td/form/div[2]/button"));
        searchButton.click();

    }
}
