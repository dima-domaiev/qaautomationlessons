package lab4.steps;

import cucumber.api.java.en.Then;
import lab3.rozetkaPages.TestRozetkaMain;
import static lab4.runner.Runner.driver;

/**
 * Created by dinamik on 6/9/2016.
 */
public class TestRozetkaSite {
    public TestRozetkaMain rozetkaMain;

    public TestRozetkaSite(){
        rozetkaMain = new TestRozetkaMain(driver);
    }

    @Then("^I see that rozetka logo is displayed$")
    public void iSeeThatRozetkaLogoIsDisplayed() throws Throwable {
        rozetkaMain.checkRozetkaLogoOnMainPage_isDispl();
    }
}
