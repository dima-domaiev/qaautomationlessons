package lab4.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lab3.rozetkaPages.TestRozetkaBasket;
import lab3.rozetkaPages.TestRozetkaMain;

import static lab4.runner.Runner.driver;

/**
 * Created by Администратор on 12.06.2016.
 */
public class RozetkaBasket {
    public TestRozetkaMain rozetkaMain;
    public TestRozetkaBasket basket;

    public RozetkaBasket(){
        rozetkaMain = new TestRozetkaMain(driver);
    }
        @When("^I see that Basket is displayed$")
    public void iSeeThatBasketIsDisplayed() throws Throwable {
        rozetkaMain.checkThatBasket_isDispl();
    }

    @Then("^I click on basket link$")
    public void iClickOnBasketLink() throws Throwable {
        basket = rozetkaMain.navigateToTheBasket();
    }

    @And("^I see that basket is empty$")
    public void iSeeThatBasketIsEmpty() throws Throwable {
        basket.checkThatBusketIsEmpty();
    }
}
