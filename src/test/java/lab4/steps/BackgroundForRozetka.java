package lab4.steps;

import cucumber.api.java.en.Given;
import lab3.rozetkaPages.TestRozetkaMain;
import lab4.runner.Runner;

import static lab4.runner.Runner.driver;

/**
 * Created by Администратор on 12.06.2016.
 */
public class BackgroundForRozetka {
    public TestRozetkaMain rozetkaMain;

    public BackgroundForRozetka(){
        rozetkaMain = new TestRozetkaMain(driver);
    }

    @Given("^I am on Rozetka main page$")
    public void iAmOnRozetakaMainPage() throws Throwable {
        if (!Runner.driver.getTitle().equals("Интернет-магазин ROZETKA™")){
            driver.get("http://rozetka.com.ua/");
        }
    }
}
