package lab4.steps;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lab3.stackowerflowPages.SingUp;
import lab3.stackowerflowPages.StMain;
import lab3.stackowerflowPages.TopQuestionPage;
import static lab4.runner.Runner.driver;

/**
 * Created by dinamik on 6/9/2016.
 */
public class TestStackoverflowSite {
    public StMain stackoverflowMain;
    public SingUp singUpPage;

    public TestStackoverflowSite(){
        stackoverflowMain = new StMain(driver);
    }

    @Given("^I on start page Stackoverflow$")
    public void iOnStartPageStackoverflow() throws Throwable {
        if(!driver.getCurrentUrl().equals("http://stackoverflow.com/"))
            driver.get("http://stackoverflow.com/");
        }

    @Then("^I see bounty is more than (\\d+)$")
    public void iSeeBountyIsMoreThan(int arg0) throws Throwable {
        stackoverflowMain.checkBounty(arg0);
    }

    @When("^I click Sign Up button$")
    public void iClickSignUpButton() throws Throwable {
        singUpPage = stackoverflowMain.navigateToSingUpPage();
    }

    @Then("^I see buttons Google and Facebook on Sing Up page$")
    public void iSeeButtonsGoogleAndFacebookOnSingUpPage() throws Throwable {
        singUpPage.googleButton();
        singUpPage.facebookButton();

    }

    @When("^I click question link$")
    public void iClickQuestionLink() throws Throwable {
        stackoverflowMain.navigateToQuestionPage();
    }

    @Then("^I see today date is displayed$")
    public void iSeeTodayDateIsDisplayed() throws Throwable {
        TopQuestionPage qPage = new TopQuestionPage(driver);
        qPage.checkQuastionDay();
    }

    @Then("^I see salary more than (\\d+)k")
    public void iSeeSalaryMoreThanK(int arg0) throws Throwable {
        stackoverflowMain.checkSalaryMoreThen100k();
    }
}
