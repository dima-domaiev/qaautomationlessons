package lab4.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import lab3.rozetkaPages.TestRozetkaCities;
import lab3.rozetkaPages.TestRozetkaMain;

import static lab4.runner.Runner.driver;

/**
 * Created by Администратор on 12.06.2016.
 */
public class RozetkaSitiesPopup {
    public TestRozetkaMain rozetkaMain;
    public TestRozetkaCities citiesPopup;

    public RozetkaSitiesPopup(){
        rozetkaMain = new TestRozetkaMain(driver);
    }

    @When("^I click on select city link$")
    public void iClickOnSelectCityLink() throws Throwable {
        rozetkaMain.closeRozetkaSubskribes();
        citiesPopup = rozetkaMain.clickCitiesLink();
    }

    @Then("^I see that select city pop-up is displayed$")
    public void iSeeThatSelectCityPopUpIsDisplayed() throws Throwable {
        citiesPopup.citiesPopup_isDispl();
    }

    @And("^I see that pop-up contains \"([^\"]*)\" city$")
    public void iSeeThatPopUpContainsCity(String arg0) throws Throwable {
        citiesPopup.citiesPopup_Contains(arg0);
    }
}
