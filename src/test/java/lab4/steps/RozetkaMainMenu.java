package lab4.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import lab3.rozetkaPages.TestRozetkaMain;

import static lab4.runner.Runner.driver;

/**
 * Created by Администратор on 12.06.2016.
 */
public class RozetkaMainMenu {
    public TestRozetkaMain rozetkaMain;

    public RozetkaMainMenu() {
        rozetkaMain = new TestRozetkaMain(driver);
    }

    @Then("^I see that Apple is displayed in main menu$")
    public void iSeeThatAppleIsDisplayedInMainMenu() throws Throwable {
        rozetkaMain.checkAppleInMenu_isDispl();
    }

    @And("^I see that MP is displayed in main menu$")
    public void iSeeThatMPIsDisplayedInMainMenu() throws Throwable {
        rozetkaMain.checkMP3InMenu_isDispl();
    }
}
