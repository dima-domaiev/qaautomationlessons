@ST_8620 @rozetka
Feature: Test rozetka site lab4

Background: 

Given I am on Rozetka main page

@SC_36445 @Logo
Scenario: Check that elements is displayed on main page
    //Given I am on Rozetka main page
Then I see that rozetka logo is displayed

@SC_36446 @MainMenu
Scenario: Check rozetka main menu
    //Given I am on Rozetka main page
Then I see that Apple is displayed in main menu
And I see that MP is displayed in main menu

@SC_36447 @citiesPopup
Scenario: Check select city pop-up
    //Given I am on Rozetka main page
When I click on select city link
Then I see that select city pop-up is displayed
And I see that pop-up contains "Киев" city
And I see that pop-up contains "Одесса" city
And I see that pop-up contains "Харьков" city

@SC_36448 @basket
Scenario: Check that the basket is empty
    //Given I am on Rozetka main page
When I see that Basket is displayed
Then I click on basket link
And I see that basket is empty