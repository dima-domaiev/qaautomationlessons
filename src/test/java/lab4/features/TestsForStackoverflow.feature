@ST_8621 @stackoverflow
Feature: Test Stackoverflow site

@SC_36449 @bounty
Scenario: check bounty is more than 300 on Stackoverflow start page
Given I on start page Stackoverflow
Then I see bounty is more than 300

@SC_36450 @SingUpPage
Scenario: check buttons Google and Facebook on Sing Up page
Given I on start page Stackoverflow
When I click Sign Up button
Then I see buttons Google and Facebook on Sing Up page

@SC_36451 @today
Scenario: check that date is today on Top Question page
Given I on start page Stackoverflow
When I click question link
Then I see today date is displayed

@SC_36452 @salary
Scenario: check salary more than 100k on start page
Given I on start page Stackoverflow
Then I see salary more than 100k