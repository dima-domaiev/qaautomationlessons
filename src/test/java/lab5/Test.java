package lab5;
import org.junit.Assert;
import org.xml.sax.SAXException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;



/**
 * Created by dinamik on 6/21/2016.
 */
public class Test {
    private Solution solution;

    public Test() {
        solution = new Solution();
    }

    @org.junit.Test


    public void checkVind() throws SAXException, ParserConfigurationException, XPathExpressionException, IOException {
        String url = "";
        String value = "";
        BufferedReader reader = new BufferedReader(new FileReader("./src/test/java/lab5/Urls.txt"));
        String line;
        List<String> lines = new ArrayList<String>();
        while ((line = reader.readLine()) != null) lines.add(line);
        List<String> list = new ArrayList<String>();
        for (String st : lines) {
            String[] tempArray = st.split("   ");
            for (String ss : tempArray) list.add(ss);
        }
        //преобразование в массив
        String[] linesAsArray = list.toArray(new String[list.size()]);

        for (int i = 0; i <= linesAsArray.length - 2; i = i + 2) {
            if (i % 2 == 0) {
                url = linesAsArray[i];
                value = linesAsArray[i + 1];
            }
            solution.sentRequest(url);
            Assert.assertTrue("",solution.nodFinder(value));


        }
    }
}

