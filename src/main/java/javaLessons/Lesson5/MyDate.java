package javaLessons.Lesson5;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by dinamik on 4/22/2016.
 */
public class MyDate {
    public static void main(String[] args) {

        javaLessons.Lesson5.MyList list = new javaLessons.Lesson5.MyList();

        for (int i = 1; i <= 30; i++) {
            list.add(i + "-04-2016");
        }

        for (int i = 0; i < 30; i++) {

            DateFormat format = new SimpleDateFormat("dd-MM-yyyy"); // формат передаваемой даты
            Date date = null;
            try {
                date = format.parse(String.valueOf(list.get(i)));
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date); // настраиваем календарь передав дату

            System.out.print(date+" - ");
            if (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY) {
                try {
                    throw new MyRuntimeException();
                } catch (MyRuntimeException e) {
                    e.printStackTrace();
                }

            } else {
                try {
                    throw new MyException();
                } catch (MyException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("-----------------------------------------------");
        }
        System.out.println(list);

        String str = "";
        for(int i=0; i<list.size(); i++){
            str = str + list.get(i) + " ";
        }

        System.out.println(str);
    }
}
