package javaLessons.Lesson5;

import java.io.PrintStream;

/**
 * Created by dinamik on 4/26/2016.
 */
public class MyException extends Exception{

    /**
     * Prints this throwable and its backtrace to the specified print stream.
     *
     * @param s {@code PrintStream} to use for output
     */
    @Override
    public void printStackTrace(PrintStream s) {
        System.out.println("Это будний день");
    }
}
