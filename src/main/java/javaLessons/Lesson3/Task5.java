package javaLessons.Lesson3;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * Created by Администратор on 10.04.2016.
 */
public class Task5 {
    public static void main(String[] args) throws Exception {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int sum = 0;
        String s = "Сумма";
        System.out.println(" Программа будет складывать введенные вами числа");
        System.out.println(" до тех пор пока вы не напишите слово \"Сумма\"");
        while(true) {
            String line = reader.readLine();
            if (line.equals(s))
                break;
            sum += Integer.parseInt(line);
            }
        System.out.println(" Сумма всех введенных чисел = " + sum);
        }
    }
