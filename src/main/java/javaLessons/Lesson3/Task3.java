package javaLessons.Lesson3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Created by Администратор on 10.04.2016.
 */
public class Task3 {
    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        System.out.printf("Введите любое целое число N: ");
        int n = Integer.parseInt(reader.readLine());
        System.out.println("Вывод на экран простых чисел: ");
        System.out.print("2 ");

        for (int i = 3; i < n; i = i + 2) {
            boolean f = true;
            for (int j = 3; j <= Math.sqrt(i); j = j +2)
                if ((i % j == 0)) {
                    f = false;
                    break;
                }
            if (f == true) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
        System.out.print("      ");
        for (int i1 = 1; i1 <=n; i1 = i1 + 2){
            if ((i1 % 2 !=0) && (i1 % 3 !=0) && (i1 % 5 !=0) && (i1 % 7 !=0)){
                System.out.print(i1 + " ");
            }
        }
    }
}

//if (i % Math.sqrt(i) == 0)
