package javaLessons.Lesson6;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import static javaLessons.Lesson6.Task1.*;

/**
 * Created by dinamik on 4/28/2016.
 */
public class Runn implements Runnable {


    /**
     * When an object implementing interface <code>Runnable</code> is used
     * to create a thread, starting the thread causes the object's
     * <code>run</code> method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method <code>run</code> is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        cop(path);
    }

    public void cop(File from) {


        File[] total = from.listFiles();
        for (File file : total) {
            if (file.isDirectory()) {
                cop(file);
            } else {
                if (file.isFile()) {
                    if (file.getName().contains(str)) {
                        try {
                            to.mkdirs();
                            File toPath = new File(to +"/"+ file.getName());
                            Files.copy(file.toPath(), toPath.toPath());
                            System.out.println("File '" + file.toPath() + "' copied to '" + to + "'");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
    }
}
