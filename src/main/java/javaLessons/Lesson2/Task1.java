package javaLessons.Lesson2;

import javax.xml.bind.SchemaOutputResolver;

/**
 * Created by dinamik on 4/6/2016.
 */
public class Task1 {
    public static void main(String[] args) {
        System.out.println("Результаты работы с операторами Арифметики: ");
        int a = 15, b = 10, c = 5;

        int d = a + b;
        int e = a - b;
        int f = a * b;
        int g = a / b;
        int h = c % a;
        System.out.println("a = 15, b = 10, c = 5");
        System.out.println("");
        System.out.println("a + b = " + d);
        System.out.println("a - b = " + e);
        System.out.println("a * b = " + f);
        System.out.println("a / b = " + g);
        System.out.println("c % a = " + h);

        System.out.println("Результаты работы с Логическими операторами: ");
        boolean i = true, j = false;
        boolean k = i || j;
        boolean l = i && j;

        System.out.println("i = true, j = false");
        System.out.println("i || j = " + k);
        System.out.println("i && j = " + l);

        System.out.println("Результаты работы с операторами Сравнения: ");

        int m = 5, n = 10;
        System.out.println("m = 5, n = 10");
        if (m < n) {
            System.out.println(m + " < " +  n );
        }
        else {
            System.out.println(n + " > " + m );
        }if (m > n) {
            System.out.println(m + " < " +  n );
        }
        else {
            System.out.println(n + " > " + m );
        }if (m <= n) {
            System.out.println(m + " <= " +  n );
        }
        else {
            System.out.println(n + " => " + m );
        }if (m >= n) {
            System.out.println(m + " <= " +  n );
        }
        else {
            System.out.println(n + " >= " + m );
        }
        System.out.println("Результаты работы с операторами Присваивания: ");
        int o, p, q ;
        o = p = q = 73;
        System.out.println("o = p = q = 73 ");
        System.out.println("Выведем \"o\" на экран. o = " + o);
        System.out.println("Выведем \"p\" на экран. p = " + p);
        System.out.println("Выведем \"q\" на экран. q = " + q);
    }


}
