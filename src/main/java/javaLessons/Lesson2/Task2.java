package javaLessons.Lesson2;

/**
 * Created by dinamik on 4/6/2016.
 */
public class Task2 {
    public static void main(String[] args) {
        System.err.println("Превод Строк в символы: ");
        System.out.println("");
        System.out.println("Строка со значением 14 перепереводится в Integer");
        String str1 = "14";
        int a = Integer.parseInt(str1);
        System.out.println("str1 = a = " + a);
        System.out.println("");

        System.out.println("Строка со значением 1456456453546235253 перепереводится в Long");
        String str2 = "1456456453546235253";
        Long b = Long.parseLong(str2);
        System.out.println("str2 = a = " + b);
        System.out.println("");

        System.out.println("Строка со значением 14.56456453546235253 перепереводится в Double");
        String str3 = "14.56456453546235253";
        Double c = Double.parseDouble(str3);
        System.out.println("str3 = a = " + c);
        System.out.println("");

        System.out.println("Строка со значениями str4 = \"true\" и str5 = \"142\" перепереводится в Boolean");
        String str4 = "true";
        String str5 = "142";
        boolean d = Boolean.parseBoolean(str4);
        boolean e = Boolean.parseBoolean(str5);
        System.out.println("str4 = d = " + d);
        System.out.println("str5 = e = " + e);
        System.out.println("");


        System.out.println("Строка со значениями str6 = \"str to Char\" переводится в массив Char-ов ");
        String str6 = "str to Char";
        char f[] = str6.toCharArray();
        System.out.print("str6 = char f = ");
        System.out.print(f);
        System.out.println("");

        System.err.println("Превод Символов в строку: ");
        System.out.println("");

        System.out.println("Integer to String ");
        int a1 = 5233;
        String s1 = Integer.toString(a1);
        System.out.println("String s1 = " + s1);

        System.out.println("double to String ");
        double a2 = 32.4e10;
        String s2 = Double.toString(a2);
        System.out.println("String s2 = " + s2);
    }
}
