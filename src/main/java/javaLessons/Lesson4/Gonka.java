package javaLessons.Lesson4;


/**
 * Created by dinamik on 4/14/2016.
 */
public class Gonka {
    public static void main(String[] args) {
        Cars car1 = new Car1(100, 3, 0.3, 0);
        int timeCar1 = car1.drive();
        System.out.println("Total time Car1 = " + timeCar1 / 60 + " minutes " + timeCar1 % 60 + " sec");


        Cars car2 = new Car2(100, 5, 0.5, 0);
        int timeCar2 = car2.drive();
        System.out.println("Total time Car2 = " + timeCar2 / 60 + " minutes " + timeCar2 % 60 + " sec");


        Cars car3 = new Car3(100, 4, 0.6, 0);
        int timeCar3 = car3.drive();
        System.out.println("Total time Car3 = " + timeCar3 / 60 + " minutes " + timeCar3 % 60 + " sec");

        System.out.println("");

        int arrTime[] = {timeCar1, timeCar2, timeCar3};
        selectionSort(arrTime);

        for (int i = 0; i < arrTime.length; i++) {
            int n;
            if (i == i) {
                n = i + 1;
                if (arrTime[i] == timeCar1) {
                    System.out.println(" Place " + n + " - " + Car1.name() + " - " + arrTime[i] / 60 + " min. " + arrTime[i] % 60 + " sec.");
                } else if (arrTime[i] == timeCar2) {
                    System.out.println(" Place " + n + " - " + Car2.name() + " - " + arrTime[i] / 60 + " min. " + arrTime[i] % 60 + " sec.");
                } else if (arrTime[i] == timeCar3) {
                    System.out.println(" Place " + n + " - " + Car3.name() + " - " + arrTime[i] / 60 + " min. " + arrTime[i] % 60 + " sec.");
                }
            }

        }
    }

    public static void selectionSort(int[] mass) {
        for (int i = 0; i < mass.length; i++) {
            int m = mass[i];
            int m_i = i;
            for (int j = i + 1; j < mass.length; j++) {
                if (mass[j] < m) {
                    m = mass[j];
                    m_i = j;
                }
            }
            if (i != m_i) {
                int temp = mass[i];
                mass[i] = mass[m_i];
                mass[m_i] = temp;
            }
        }
    }
}

