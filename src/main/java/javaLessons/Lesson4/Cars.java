package javaLessons.Lesson4;

public abstract class Cars {
    double maxSpeed;
    double acceleration;
    double mobility;
    double V0 = 0;

    public Cars(double maxSpeed, double acceleration, double mobility, double V0) {
        this.maxSpeed = maxSpeed;
        this.acceleration = acceleration;
        this.mobility = mobility;
        this.V0 = V0;
    }

    public int drive() {
        double acc;
        double time = 0;
        double t1;
        double t2 = 0;
        for (int i = 1; i <= 20; i++) {
            double afterAcc;
            acc = (((Math.pow(maxSpeed, 2)) - (Math.pow(V0, 2))) / (2 * acceleration));
            if (acc < 2000) {
                afterAcc = Math.sqrt((Math.pow(V0, 2)) + (2 * acceleration * 2000));
                t1 = (afterAcc - V0) / acceleration;
            } else {
                afterAcc = maxSpeed;
                t1 = (afterAcc - V0) / acceleration;
                t2 = (2000 - acc) / maxSpeed;
            }
            time += t1 + t2;
            turn();
        }

        int timeInt = (int) time;
        return timeInt;
    }

    public abstract void turn();
}
