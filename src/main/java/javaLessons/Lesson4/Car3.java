package javaLessons.Lesson4;

public class Car3 extends Cars {

    public static String name() {
        String name = "Car3";
        return name;
    }

    public Car3(double maxSpeed, double acceleration, double mobility, double V0) {
        super(maxSpeed, acceleration, mobility, V0);
    }

    @Override
    public void turn() {
        if (V0 == maxSpeed) {
            maxSpeed = maxSpeed * 1.1;
        }
    }
}
