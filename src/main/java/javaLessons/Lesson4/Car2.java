package javaLessons.Lesson4;

public class Car2 extends Cars {

    public static String name() {
        String name = "Car2";
        return name;
    }

    public Car2(double maxSpeed, double acceleration, double mobility, double V0) {
        super(maxSpeed, acceleration, mobility, V0);
    }

    @Override
    public void turn() {
        if (V0 * mobility < maxSpeed / 2) {
            if (acceleration < acceleration / 2) {
                acceleration = acceleration * 2;
            }
        }
    }
}
