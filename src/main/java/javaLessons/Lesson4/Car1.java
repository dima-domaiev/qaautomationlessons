package javaLessons.Lesson4;

public class Car1 extends Cars {


    public static String name() {
        String name = "Car1";
        return name;
    }

    public Car1(double maxSpeed, double acceleration, double mobility, double V0) {
        super(maxSpeed, acceleration, mobility, V0);
    }


    @Override
    public void turn() {
        if (V0 > maxSpeed / 2) {
            mobility = Math.abs(maxSpeed / 2 - V0) * 0.005;
        }


    }


}


