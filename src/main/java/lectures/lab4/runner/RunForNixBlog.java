package lectures.lab4.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by student on 6/2/2016.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        glue ="lectures\\lab4\\steps",
        features = "src\\test\\java\\lectures\\lab4\\features",
        tags = "@blog"
)
public class RunForNixBlog {
    public static WebDriver driver;

    @BeforeClass
    public static void setUp(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        if(!driver.getCurrentUrl().equals("http://www.nixsolutions.com/"))
            driver.get("http://www.nixsolutions.com/");
    }
    @AfterClass
    public static void testDown(){
        driver.quit();
    }
}

