package lectures.lab3.pom;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by student on 5/31/2016.
 */
public class MainPage {
    private WebDriver driver;

    public MainPage(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }

    @FindBy(xpath = ".//*[@id='listMenuRoot']/li[3]/a")
    public WebElement linkToDeliv;

    public Deliv navToDeliv(){
        linkToDeliv.click();
        return new Deliv(driver);
    }
}
