package lectures.lab3.pom;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by student on 5/31/2016.
 */
public class Deliv {
    private WebDriver driver;

    public Deliv(WebDriver driver){
        PageFactory.initElements(driver,this);
        this.driver=driver;
    }

    @FindBy(id = "page-title")
    public WebElement lbl_pagetitle;
    public void chekLableDostavka(){
        Assert.assertTrue("borodavo4ra",lbl_pagetitle.getText().contains("Доставка"));
    }
}
