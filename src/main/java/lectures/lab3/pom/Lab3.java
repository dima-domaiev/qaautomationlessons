package lectures.lab3.pom;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by student on 5/31/2016.
 */
public class Lab3 {
    private static WebDriver driver;
    private static MainPage mainPage;

    @BeforeClass
    public static void SetUp() {
        driver = new FirefoxDriver();
        driver.get("http://elmir.ua/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        mainPage = new MainPage(driver);
    }

    @AfterClass
    public static void tearDown() {
        driver.close();
    }

    @Test
    public void test1() {
        Deliv DelPage = mainPage.navToDeliv();
        DelPage.chekLableDostavka();
    }
}
