package lectures.lab5;

import org.junit.Assert;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by dinamik on 6/14/2016.
 */
public class ExampleLab5 {
    public static void main(String[] args) {
        try {
            URL url = new URL("http://api.met.no/weatherapi/sunrise/1.0/?lat=70;lon=19;date=2011-06-07");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");


            if (conn.getResponseCode() != 203) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");
            String temp = "";
            while ((output = br.readLine()) != null) {
                temp += output;
                System.out.println(output);
                Assert.assertTrue(output.contains("First quarter"));
            }
            conn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

