package lab4.runner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by dinamik on 6/7/2016.
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        glue = "lab4\\steps",
        features = "src\\test\\java\\lab4",
        tags = "@rozetka,@stackoverflow"
//        tags = "@stackoverflow"
//        tags = "@rozetka, @citiesPopup"
//        tags = "@basket"
//        tags = "@MainMenu"
)
public class Runner {
    public static WebDriver driver;


    @BeforeClass
    public static void setUp(){
        driver = new FirefoxDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void testDown(){
        driver.quit();
    }

}
