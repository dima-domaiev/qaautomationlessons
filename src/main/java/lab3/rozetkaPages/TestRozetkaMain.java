package lab3.rozetkaPages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by dinamik on 5/30/2016.
 */
public class TestRozetkaMain {
    private WebDriver driver;

    public TestRozetkaMain(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[contains(@src,'99.png')]")
    public WebElement rozetkaLogo;

    public void checkRozetkaLogoOnMainPage_isDispl() {
        Assert.assertTrue("rozetkaLogo is not displayed", rozetkaLogo.isDisplayed());
    }

    @FindBy(xpath = ".//*[@id='m-main']/li[contains(a,'Apple')]/a")
    public WebElement rozetkaApple;

    public void checkAppleInMenu_isDispl() {
        Assert.assertTrue("Apple is not displayed in main menu", rozetkaApple.getText().equals("Apple"));
    }


    @FindBy(xpath = ".//*[@id='m-main']/li[contains(a,'MP3')]/a")
    public WebElement rozetkaMp3;

    public void checkMP3InMenu_isDispl() {
        Assert.assertTrue("MP3 is not displayed in main menu", rozetkaMp3.getText().equals("Телефоны, MP3"));
    }

    @FindBy(xpath = ".//*[@id='city-chooser']/a/span[text()='Выберите город']")
    public WebElement rozetkaCities;

    @FindBy(xpath = ".//*[@id='SubscribePushNotificationPanel']/div/div[@class='notificationPanelCross']")
    public WebElement closePopupNearCitiesLink;

    public void closeRozetkaSubskribes() {
        try {
            closePopupNearCitiesLink.click();
        }catch (Exception e){
            System.out.println("Subskribes popup is not found");
        }

    }

    public void rozetkaCities_isDispl() {
        Assert.assertTrue("rozetkaCities is not displayed", rozetkaCities.isDisplayed());
    }

    public TestRozetkaCities clickCitiesLink() {
        rozetkaCities.click();
        return new TestRozetkaCities(driver);
    }

    @FindBy(xpath = ".//*[contains(@id,'cart_block')]/a")
    public WebElement rozetkaBasket;

    public void checkThatBasket_isDispl() {
        Assert.assertTrue("rozetkaBasket is not displayed", rozetkaBasket.isDisplayed());
    }

    public TestRozetkaBasket navigateToTheBasket() {
        rozetkaBasket.click();
        return new TestRozetkaBasket(driver);
    }
}
