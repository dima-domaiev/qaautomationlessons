package lab3.rozetkaPages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by dinamik on 5/30/2016.
 */
public class TestRozetkaCities {
    private WebDriver driver;

    public TestRozetkaCities(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@id='city-chooser']/div/div")
    public WebElement citiesPopup;

    public void citiesPopup_isDispl() {
        Assert.assertTrue("citiesPopup is displayed", citiesPopup.isDisplayed());
    }

    public void citiesPopup_Contains(String city) {
        Assert.assertTrue("citiesPopup is not contains some cities", citiesPopup.getText().contains(city));
    }

    @FindBy(xpath = ".//*[@id='city-chooser']/div/div/a/img")
    public WebElement closePopupSelCity;

    public TestRozetkaCities closePopupSelCity_Click() {
        closePopupSelCity.click();
        return new TestRozetkaCities(driver);
    }
}
