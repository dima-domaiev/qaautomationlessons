package lab3.rozetkaPages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by dinamik on 5/30/2016.
 */
public class TestRozetkaBasket {
    public WebDriver driver;


    public TestRozetkaBasket(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[contains(@id,'cart_block')]/a")
    public WebElement emptyBasket;

        public void checkThatBusketIsEmpty() {
        Assert.assertTrue("Basket is not empty", emptyBasket.isDisplayed());
    }
}
