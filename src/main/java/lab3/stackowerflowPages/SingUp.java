package lab3.stackowerflowPages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by dinamik on 6/6/2016.
 */
public class SingUp {
    public WebDriver driver;

    public SingUp(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[contains(text(),'Google')]/..")
    public WebElement loginFromGoogleButton;

    public void googleButton() {
        Assert.assertTrue("", loginFromGoogleButton.isDisplayed());
    }

    @FindBy(xpath = ".//*[contains(text(),'Facebook')]/..")
    public WebElement loginFromFacebookButtonl;

    public void facebookButton() {
        Assert.assertTrue("", loginFromGoogleButton.isDisplayed());
    }
}
