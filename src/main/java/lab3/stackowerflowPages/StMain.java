package lab3.stackowerflowPages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by dinamik on 6/6/2016.
 */
public class StMain {
    public WebDriver driver;

    public StMain(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[@id='tabs']/a/span")
    public WebElement bountyIndicatorMoreThen300;

    public void checkBounty(int val) {
        String value = bountyIndicatorMoreThen300.getText();
        Assert.assertTrue("failed", (Integer.parseInt(value) > val));
    }

    @FindBy(xpath = ".//*[@id='tell-me-more']")
    public WebElement buttonSingUp;

    public SingUp navigateToSingUpPage() {
        buttonSingUp.click();
        return new SingUp(driver);
    }

    @FindBy(xpath = ".//*[@class='relativetime']/../../../h3/a")
    public WebElement questionHyperlink;

    public void navigateToQuestionPage() {
        questionHyperlink.click();
    }


    @FindBy(xpath = ".//*[@class='title']")
    public WebElement jobs;

    public void checkSalaryMoreThen100k() {
        String salary = jobs.getText();

        Pattern p = Pattern.compile("-?\\d+");
        Matcher m = p.matcher(salary);
        while (m.find()) {
            System.out.println(m.group());
            salary = m.group();
        }
        int i = Integer.parseInt(salary);
        Assert.assertTrue("Salary less then 100 K", i > 100);
    }
}
