package lab3.stackowerflowPages;

import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by dinamik on 6/6/2016.
 */
public class TopQuestionPage {
    public WebDriver driver;

    public TopQuestionPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[contains(@title,'2016')]/b")
    public WebElement asked;

    public void checkQuastionDay() {
        Assert.assertTrue("nezhdano4ka", asked.getText().equals("today"));
    }
}
