package z_draft;

import org.openqa.selenium.WebDriver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by dinamik on 6/14/2016.
 */
public class ExampleLab5_Copy {
    private static String fileName = "./src/test/java/z_draft/whether.xml";
    public static String temp = "";
    public WebDriver driver;




    public static void main(String[] args) {
        try {
            URL url = new URL("http://api.met.no/weatherapi/locationforecast/1.9/?lat=49.9935;lon=36.2304");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");


            if (conn.getResponseCode() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + conn.getResponseCode());
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(
                    (conn.getInputStream())));

            String output;
            System.out.println("Output from Server .... \n");

            while ((output = br.readLine()) != null) {
                temp += output;
                System.out.println(output);
                  //Assert.assertTrue("not found",output.contains("pointData"));
            }

            ClassForFile export = new ClassForFile();
            if (output != null) {
                export.write(fileName, temp);
            }

            conn.disconnect();

        } catch (MalformedURLException e) {

            e.printStackTrace();

        } catch (IOException e) {

            e.printStackTrace();

        }
    }


}

