package z_draft;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by dinamik on 6/15/2016.
 */
public class ClassForFile {
    private static String fileName = "./src/test/java/z_draft/whether.xml";

    public void write(String fileName, String text) {
        //Определяем файл
        File file = new File(fileName);

        try {
            //проверяем, что если файл не существует то создаем его
            if (!file.exists()) {
                file.createNewFile();
            }

            //PrintWriter обеспечит возможности записи в файл
            PrintWriter out = new PrintWriter(file.getAbsoluteFile());

            try {
                //Записываем текст в файл
                out.print(text);
            } finally {
                out.close();
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
